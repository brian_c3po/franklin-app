This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to run

clone the repo locally. 

if first time running run `npm install`

to start the application run `npm start`

## next steps

build out functionality for other CRUD commands

add unit tests

add styling



