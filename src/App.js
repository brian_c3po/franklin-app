import React, { Component } from 'react';
import axios from 'axios'
import './App.css';

class App extends Component {
  constructor(props){
    super(props)
    this.state= {
      selected: '',
      data: []
    }
  }

  fetchAndLoad(table){
    fetchTable(table)
      .then(data => {
        this.setState({
          selected: table,
          data: data
        })
      })
  }

  renderRow(row) {
    let keys = Object.keys(row)
    return (<tr>
       {
         keys.map(key => {
           return <td> {row[key]} </td>
         })
       }
    </tr>)
  }

  renderHeader(data){
    if(data[0]){
      let keys = Object.keys(data[0])
      return (
        <tr>
          {
            keys.map(key => {
              return <th> {key} </th>
            })
          }
        </tr>
      )
    }
  }

  render() {
    return (
      <div className="App">
        <div>
          <select onChange = {(e) => this.fetchAndLoad(e.target.value)}>
            <option></option>
            <option value='users'> user </option>
            <option value='items'> items </option>
            <option value='orders'> orders </option>
            <option value='order-items'> order_items </option>
          </select>
        </div>
        <div className="data-table">
          <table>
            { this.renderHeader(this.state.data) }
            {
              this.state.data.map( record => {
                return this.renderRow(record)
              })
            }
          </table>
        </div>
      </div>
    );
  }
}

export default App;

function fetchTable(tableName){
  console.log('fetching');
  return axios.get(`http://localhost:3002/api/v1/${tableName}`)
  .then((res) => {
    let data = res.data;
    return data
  })
  .catch(e => console.log(e))
}
